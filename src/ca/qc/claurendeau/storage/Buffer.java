package ca.qc.claurendeau.storage;

import java.util.Iterator;
import java.util.LinkedList;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
	private LinkedList<Element> queue;
    private int capacity;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.queue = new LinkedList<Element>();
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        String string = null;
        
        Iterator<Element> it = this.queue.iterator();
        while(it.hasNext()) {
        	string += it.next().getData();
        }
        
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return this.capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return this.queue.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
    	return this.queue.isEmpty();
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
    	return (this.queue.size() >= capacity ? true : false);
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
    	if(this.queue.size() < capacity) {
        	this.queue.add(element);
        }else {
        	throw new BufferFullException();
        }
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
    	Element temp = null;
    	
    	if(this.queue.size() > 0) {
    		temp = this.queue.pollFirst();
    	}
    	else {
    		throw new BufferEmptyException();
    	}
        return temp;
    }
}
