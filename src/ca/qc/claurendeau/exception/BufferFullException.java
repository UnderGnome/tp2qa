package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
	
	public BufferFullException() {}
	
	public String toString() {
		return BufferFullException.class.getCanonicalName() + " : Buffer is Full";
	}
}
